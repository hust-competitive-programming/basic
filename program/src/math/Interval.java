package math;

public class Interval{
	public int low, high;
	public int id=-1;
	public Interval(int low, int high) {
		this.low = low;
		this.high = high;
	}
	public Interval(int low, int high, int id) {
		this.low = low;
		this.high = high;
		this.id = id;
	}
	public boolean doOverlap(Interval other) {
		if (low <= other.high && other.low <= high)
			return true;
		return false;
	}
	
	public int compareTo(Interval other) {
		if (high < other.low) return -1;
		else if(low > other.high) return 1;
		else if (id == other.id) return 0;
		else return -1;
	}
}

