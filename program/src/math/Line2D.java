package math;

public class Line2D {
	public Double a;
	public Double b;
	public Double c;
	
	public Line2D(Double a, Double b, Double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public Line2D(Line2D other) {
		this.a = other.a;
		this.b = other.b;
		this.c = other.c;
	}
	
	public Double computes(Point2D p) {
		return (Double) a*p.x + b*p.y + c;
	}
	
	public boolean onLine(Point2D p) {
		return Math.abs(computes(p)) < 1.0e-9;
	}
	
	public Point2D transforms() {
		return new Point2D(-a/b, c/b);
	}
	
	public Point2D getIntersection(Line2D other) {
		Double x = (c*other.a - other.c*a) / (other.b*a + other.a*b);
		Double y = (-c - a*x)/b;
		return new Point2D(x, y);
	}
}
