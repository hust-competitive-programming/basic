package math;

public class Rectangle {
	public Point2D topLeft;
	public Point2D bottomRight;
	public int id;
	public Rectangle(Point2D p1, Point2D p2) {
		if(p1.x > p2.x) {
			topLeft = p2;
			bottomRight = p1;
		}
		topLeft = p1;
		bottomRight = p2;
	}
	
	public Rectangle(Point2D p1, Point2D p2, int id) {
		if(p1.x > p2.x) {
			topLeft = p2;
			bottomRight = p1;
		}
		topLeft = p1;
		bottomRight = p2;
		this.id = id;
	}
	
	public Interval getInter() {
		return new Interval(bottomRight.y, topLeft.y, id);
	}
	
	public int getWidth() {
		return bottomRight.x - topLeft.x;
	}
	
	public int getHeight() {
		return bottomRight.y - topLeft.y;
	}
	
//	public Rectangle convertToCentre() {
//		return 
//	}
	public int getArea() {
		return getWidth() * getHeight();
	}
	
	public boolean compares(Rectangle other) {
		return topLeft.comparesTo(other.topLeft);
	}
	
	public int iou(Rectangle other) {
		int x1 = Math.max(topLeft.x, other.topLeft.x);
		int x2 = Math.min(bottomRight.x, other.bottomRight.x);
		int y1 = Math.max(topLeft.y, other.topLeft.y);
		int y2 = Math.min(bottomRight.y, other.bottomRight.y);
		
		return (x2 - x1) * (y2 - y1);
	}
}
