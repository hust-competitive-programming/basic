package math;
import java.util.ArrayList;
import math.Interval;

public class IntervalTree {
	public class ITNode{
		Interval i;
		int max;
		ITNode left, right;
		int size;
		public ITNode(Interval i) {
			this.i = i;
			this.max = i.high;
			this.left = this.right = null;
			size = 1;
		}
	}
	public ITNode root;
	public ITNode insert(ITNode root, Interval i) {
		if (root == null)
			return new ITNode(i);
		int l = root.i.low;
		if (i.low < l)
			root.left = insert(root.left, i);
		else
			root.right = insert(root.right, i);
		if (root.max < i.high)
			root.max = i.high;
		return root;
	}
	
	public void updateSize(ITNode root) {
		
	}
	
	public Interval overlapSearch(ITNode root, Interval i) {
		if (root == null) return null;
		if (root.i.doOverlap(i))
			return root.i;
		if (root.left != null && root.left.max >= i.low)
			return overlapSearch(root.left, i);
		return overlapSearch(root.right, i);
	}
	
	public void inorder(ITNode root) 
	{ 
	    if (root == null) return; 	  
	    inorder(root.left); 
	    System.out.println("[" + root.i.low + "," + root.i.high + "]" + " max= " + root.max);
	    inorder(root.right); 
	} 
	
	public int size() {
		return size(root);
	}
	
	public ITNode remove(ITNode root, Interval i) {
		if (root == null) return null;
		int cmp = i.compareTo(root.i);
		if (cmp < 0) root.left = remove(root.left, i);
		else if(cmp > 0) root.right = remove(root.right, i);
		else root = joinLR(root.left, root.right);		
	}
	
	public ITNode joinLR(ITNode a, ITNode b) {
		if (a == null) return b;
		if (b == null) return a;
		
		if (Math.random() * ())
	}
	
//	public static void main(String [] args) {
//		IntervalTree test = new IntervalTree();
//		ITNode root = null;
//		ArrayList<Interval> is = new ArrayList<Interval>();
//		is.add(new Interval(15, 20));
//		is.add(new Interval(10, 30));
//		is.add(new Interval(17, 19));
//		is.add(new Interval(5, 20));
//		is.add(new Interval(12, 15));
//		is.add(new Interval(30, 40));
//		int n = is.size();
//		for(Interval i: is) {
//			root = test.insert(root, i);
//		}
//		System.out.println("Inorder traversal of constructed Interval Tree is\n"); 
//	    test.inorder(root); 
//	    Interval x = new Interval(6, 7);
//	    System.out.println("Searching for interval [" + x.low + "," + x.high + "]"); 
//	    Interval res = test.overlapSearch(root, x);
//	    if (res == null)
//	    	System.out.println("No Overlapping Interval");
//	    else
//	    	System.out.println("Overlaps with [" + res.low +  ", " + res.high + "]");
//	}	
}
