package math;
import java.util.ArrayList;
import java.util.List;

public final class DoublePoint2D implements CGObject {
	
	public static final DoublePoint2D ORIGIN = new DoublePoint2D();
	public Double x, y;
	public Double hash;	
	
	public DoublePoint2D() {
		this(0.0, 0.0);
	}
	
	public DoublePoint2D(Double min, Double max, Double offset) {
		x = (Double) (Math.random() * (max - min) + max);
		y = (Double) (Math.random() * (max -min - offset) + offset + min);
	}
	
	public DoublePoint2D(Double x, Double y) {
		this.x = x;
		this.y = y;
//		hash = (x.hashCode() * 23) ^ (y.hashCode() * 17);
	}
	
	public DoublePoint2D(DoublePoint2D other) {
		x = other.x;
		y = other.y;
	}
	
	public Double distaince(DoublePoint2D other) {
		return Math.sqrt((x - other.x) * (x - other.x) + 
				(y - other.y) * (y - other.y));
	}
	
	public boolean equals(DoublePoint2D other) {
		return Math.abs(y - other.y) < 1.0e-9 && Math.abs(x - other.x) < 1.0e-9;
	}
	
	public boolean notEquals(DoublePoint2D other) {
		return Math.abs(y - other.y) > 1.0e-9 || Math.abs(x - other.x) > 1.0e-9;
	}
	// 0 --> this, q and r are colinear 
	// 1 --> Clockwise 
	// 2 --> Counterclockwise 
	public int orientation(DoublePoint2D q, DoublePoint2D r) {
		Double val = (q.y - y) * (r.x - x)  - (q.x - x) * (r.y - y);
		if(Math.abs(val) < 1.0e-9) return 0; // colinear
		return Math.abs(val) > 1.0e-9? 1: 2;
	}
	
	public boolean comparesTo(DoublePoint2D other) {
		if (x < other.x - 1.0e-9)
			return true;
		else if (x > other.x + 1.0e-9)
			return false;
		else if (y < other.y)
			return true;
		return false;	
	}
}
