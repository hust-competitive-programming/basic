package math;

public class DoubleInterval{
	public Double low, high;
	public int id=-1;
	public DoubleInterval(Double low, Double high) {
		this.low = low;
		this.high = high;
	}
	public DoubleInterval(Double low, Double high, int id) {
		this.low = low;
		this.high = high;
		this.id = id;
	}
	public boolean doOverlap(DoubleInterval other) {
		if (low <= other.high - 1.0e-9 && other.low <= high - 1.0e-9)
			return true;
		return false;
	}
	
	public int compareTo(DoubleInterval other) {
		if (high < other.low - 1.0e-9) return -1;
		else if(low > other.high + 1.0e-9) return 1;
		else if (id == other.id) return 0;
		else return -1;
	}
}

