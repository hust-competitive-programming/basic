package math;

public final class LineSegment2D implements CGObject{
	public Point2D leftPoint, rightPoint;
	public Point2D directionalVector;
	public static int index = 0;
	public int id = -1;
	public int hash;
	
	public LineSegment2D() {
		this(new Point2D(0.0, 0.0), new Point2D(0.0, 0.0));
	}
	
	public LineSegment2D(LineSegment2D other) {
		leftPoint = other.leftPoint;
		rightPoint = other.rightPoint;
		id = index;
		index += 1;
		directionalVector = new Point2D(rightPoint.x - leftPoint.x, rightPoint.y - rightPoint.y);
	}
	
	
	public LineSegment2D(Point2D X, Point2D Y) {
		leftPoint = new Point2D(X);
		rightPoint = new Point2D(Y);
		id = index;
		index += 1;
		directionalVector = new Point2D(rightPoint.x - leftPoint.x, rightPoint.y - rightPoint.y);
	}
	
	public boolean onSegment(Point2D p) {
		// Considering that p is on this;
		if (p.x <= Math.max(leftPoint.x, rightPoint.x) && 
			p.x >= Math.min(leftPoint.x, rightPoint.x) && 
			p.y <= Math.max(leftPoint.y, rightPoint.y) && 
			p.y >= Math.min(leftPoint.y, rightPoint.y))
			return true;
		return false;
	}
	
	public boolean isVertical() {
		return Math.abs(leftPoint.x - rightPoint.x) < 1.0e-9;
	}
	
	public boolean isHorizontal() {
		return Math.abs(leftPoint.y - rightPoint.y) < 1.0e-9;
	}
	
	public void printLineSegmentDetail() {
		System.out.println("(" + leftPoint.x + ", " + leftPoint.y + "), (" + rightPoint.x + ","  + rightPoint.y + ")");
	}
	
	
	public Point2D intersects(LineSegment2D other) {
		// Find the four orientations needed for general and special cases
		int o1 = leftPoint.orientation(rightPoint, other.leftPoint);
		int o2 = leftPoint.orientation(rightPoint, other.rightPoint);
		int o3 = other.leftPoint.orientation(other.rightPoint, leftPoint);
		int o4 = other.leftPoint.orientation(other.rightPoint, rightPoint);
		
		// General case
		if (Math.abs(o1) < 1.0e-9 && onSegment(other.leftPoint))
			return other.leftPoint;
		if (Math.abs(o2) < 1.0e-9 && onSegment(other.rightPoint))
			return other.rightPoint;
		if (Math.abs(o3) < 1.0e-9 && onSegment(leftPoint))
			return leftPoint;
		if (Math.abs(o4) < 1.0e-9 && onSegment(rightPoint))
			return rightPoint;
		if (Math.abs(o1 - o2) > 1.0e-9 && Math.abs(o3 - o4) > 1.0e-9) {
			Point2D intersectionaPoint;
			// ax + by +c = 0
			Double a1 = directionalVector.y;
			Double b1 = directionalVector.x;
			
			Double a2 = other.directionalVector.y;
			Double b2 = other.directionalVector.x;
			Double c3 = other.directionalVector.x * other.leftPoint.y - other.directionalVector.y * other.leftPoint.x;
			
			Double k = (0 - c3 - leftPoint.x*a2 - leftPoint.y*b2) / (a1*a2 + b1*b2);
			
			intersectionaPoint = new Point2D(a1*k + leftPoint.x, b1*k + leftPoint.y);
			return intersectionaPoint;
		}
		return null;
	}
	
	public final boolean interects(LineSegment2D other) {
		int o1 = leftPoint.orientation(rightPoint, other.leftPoint);
		int o2 = leftPoint.orientation(rightPoint, other.rightPoint);
		int o3 = other.leftPoint.orientation(other.rightPoint, leftPoint);
		int o4 = other.leftPoint.orientation(other.rightPoint, rightPoint);
		if (Math.abs(o1) < 1.0e-9 && onSegment(other.leftPoint))
			return true;
		if (Math.abs(o2) < 1.0e-9 && onSegment(other.rightPoint))
			return true;
		if (Math.abs(o3) < 1.0e-9 && onSegment(leftPoint))
			return true;
		if (Math.abs(o4) < 1.0e-9 && onSegment(rightPoint))
			return true;
		if (Math.abs(o1 - o2) > 1.0e-9 && Math.abs(o3 - o4) > 1.0e-9) 
			return true;
		return false;
	}
	
	public boolean equals(LineSegment2D other) {
		return leftPoint.equals(other.leftPoint) && rightPoint.equals(other.rightPoint);
	}
	
	public boolean comparesAccordingToLeftPoint(LineSegment2D other) {
		return leftPoint.comparesTo(other.leftPoint);
	}
	
	public boolean comparesAccordingToRightPoint(LineSegment2D other) {
		return rightPoint.comparesTo(other.rightPoint);
	}
	
	public Point2D newIntersects(LineSegment2D other) {
		// Line AB represented as a1x + b1y = c1 
		double a1 = rightPoint.y - leftPoint.y;
		double b1 = leftPoint.x - rightPoint.x;
		double c1 = a1 * leftPoint.x + b1 * leftPoint.y;
		
		// Line AB represented as a1x + b1y = c1 
		double a2 = other.rightPoint.y - other.leftPoint.y;
		double b2 = other.leftPoint.x - other.rightPoint.x;
		double c2 = a2 * other.leftPoint.x + b2 * other.leftPoint.y;
		
		double determinant = a1 * b2 - a2 * b1;
		if (Math.abs(determinant) < 1.0e-9) {
			return null;
		}
		double x = (b2*c1 - b1*c2)/determinant; 
        double y = (a1*c2 - a2*c1)/determinant; 
        return new Point2D(x, y); 
	}
	
}
