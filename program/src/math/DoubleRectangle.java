package math;

public class DoubleRectangle {
	public DoublePoint2D topLeft;
	public DoublePoint2D bottomRight;
	public int id;
	public DoubleRectangle(DoublePoint2D p1, DoublePoint2D p2) {
		if(p1.x > p2.x) {
			topLeft = p2;
			bottomRight = p1;
		}
		topLeft = p1;
		bottomRight = p2;
		
	}
	
	public DoubleRectangle(DoublePoint2D p1, DoublePoint2D p2, int id) {
		if(p1.x > p2.x) {
			topLeft = p2;
			bottomRight = p1;
		}
		topLeft = p1;
		bottomRight = p2;
		this.id = id;
	}
	
	public DoubleInterval getInter() {
		return new DoubleInterval(bottomRight.y, topLeft.y, id);
	}
	
	public Double getWidth() {
		return bottomRight.x - topLeft.x;
	}
	
	public Double getHeight() {
		return bottomRight.y - topLeft.y;
	}
	
	public DoublePoint2D getPointCentre() {
		return new DoublePoint2D((topLeft.x + bottomRight.x)/2, (topLeft.y + bottomRight.y)/2);
	}
	
//	public Rectangle convertToCentre() {
//		return 
//	}
	public Double getArea() {
		return getWidth() * getHeight();
	}
	
	public boolean compares(DoubleRectangle other) {
		return topLeft.comparesTo(other.topLeft);
	}
	
	public Double iou(DoubleRectangle other) {
		Double x1 = Math.max(topLeft.x, other.topLeft.x);
		Double x2 = Math.min(bottomRight.x, other.bottomRight.x);
		Double y1 = Math.max(topLeft.y, other.topLeft.y);
		Double y2 = Math.min(bottomRight.y, other.bottomRight.y);
		
		return (x2 - x1) * (y2 - y1);
	}
}
