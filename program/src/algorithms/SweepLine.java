package algorithms;
import java.util.ArrayList;

import java.util.PriorityQueue;

import math.Point2D;
import utils.RangeSearch;
import math.LineSegment2D;

public class SweepLine {
	
	public static class Event implements Comparable<Event>{
		Double time;
		LineSegment2D lineSegment;
		
		public Event(Double time, LineSegment2D lineSegment) {
			this.time = time;
			this.lineSegment = lineSegment;
		}

		@Override
		public int compareTo(Event other) {
			// TODO Auto-generated method stub
			if (this.time < other.time - 1.0e-9) return -1;
			else if (this.time > other.time + 1.0e-9) return 1;
			return 0;
		}
		
	}
	
	public static ArrayList<LineSegment2D> randomSegments(int n, Double min, Double max, Double offset){
		ArrayList<LineSegment2D> randomLineSegments = new ArrayList<LineSegment2D>();
		LineSegment2D randomLineSegment;
		for(int i = 0; i < n; i++) {
			Double x1 = (Double) (Math.random() * (max - min) + min);
			Double x2 = (Double) (Math.random() * (max - x1 - offset) + x1 + offset);
			Double y1 = (Double) (Math.random() * (max - min) + min);
			Double y2 = (Double) (Math.random() * (max - y1 - offset) + y1 + offset);
			if (Math.random() < 0.5) randomLineSegment = new LineSegment2D(new Point2D(x1, y1), new Point2D(x2, y2));
			else randomLineSegment = new LineSegment2D(new Point2D(x1, y1), new Point2D(x2, y2));
			randomLineSegments.add(randomLineSegment);
		}
		return randomLineSegments;
	}
	
	public void solver() {
		int N = 10;
		Double INFINITY = Double.MAX_VALUE;		
		ArrayList<Point2D> intersections = new ArrayList<Point2D>();
		ArrayList<LineSegment2D> lineSegments = randomSegments(N, 0.0, 10.0, 3.0);

		
		PriorityQueue<SweepLine.Event> minHeap = new PriorityQueue<SweepLine.Event>();
		for (int i = 0; i < N; i++) {
			if (lineSegments.get(i).isVertical()) {
//				lineSegments.get(i).printLineSegmentDetail();
				Event e = new Event(lineSegments.get(i).leftPoint.x, lineSegments.get(i));
				minHeap.add(e);
			}else {
				Event e1 = new Event(lineSegments.get(i).leftPoint.x, lineSegments.get(i));
				Event e2 = new Event(lineSegments.get(i).rightPoint.x, lineSegments.get(i));
				minHeap.add(e1);
				minHeap.add(e2);
			}
		}
//		System.out.println(minHeap.isEmpty());
		
		// run sweep-line algorithm
		RangeSearch balancingBinarySearchTree = new RangeSearch();
		while (!minHeap.isEmpty()) {
			Event e = minHeap.poll();
			e.lineSegment.printLineSegmentDetail();
			Double sweep = e.time;
			LineSegment2D lineSegment = e.lineSegment;
			
			// vertical segment
			if (lineSegment.isVertical()) {
				// a bit of a hack here - use infinity to handle degenerate intersections
				LineSegment2D lineSeg1 = new LineSegment2D(new Point2D(-INFINITY, lineSegment.leftPoint.y), 
														   new Point2D(-INFINITY, lineSegment.leftPoint.y));
				LineSegment2D lineSeg2 = new LineSegment2D(new Point2D(+INFINITY, lineSegment.rightPoint.y), 
						                                   new Point2D(+INFINITY, lineSegment.rightPoint.y));
				
				Iterable<LineSegment2D> list = balancingBinarySearchTree.range(balancingBinarySearchTree.currentRoot, lineSeg1, lineSeg2);
				for (LineSegment2D lineSeg: list) {
					System.out.print("Intersection between " + lineSegment + " " + lineSeg);
				}
			}
			// if sweep is left point of line segment
			else if (Math.abs(sweep - lineSegment.leftPoint.x) < 1.0e-9) {
				System.out.println(Math.abs(sweep - lineSegment.leftPoint.x));
				balancingBinarySearchTree.currentRoot = balancingBinarySearchTree.insert(balancingBinarySearchTree.currentRoot, lineSegment);
				LineSegment2D lineSeg1 = new LineSegment2D(new Point2D(-INFINITY, lineSegment.leftPoint.y), 
						   									new Point2D(-INFINITY, lineSegment.leftPoint.y));
				Iterable<LineSegment2D> list = balancingBinarySearchTree.range(balancingBinarySearchTree.currentRoot, lineSeg1, lineSegment);
				for (LineSegment2D lineSeg11: list) {
					for (LineSegment2D lineSeq2: list) {
						if(!lineSeg11.equals(lineSeq2))
							if (lineSeg11.newIntersects(lineSeq2) != null) {
								balancingBinarySearchTree.currentRoot = balancingBinarySearchTree.remove(balancingBinarySearchTree.currentRoot, lineSegment);

							}
					}
				}

			}		
			else if (Math.abs(sweep - lineSegment.rightPoint.x) < 1.0e-9)
				balancingBinarySearchTree.currentRoot = balancingBinarySearchTree.remove(balancingBinarySearchTree.currentRoot, lineSegment);
			else {
//				Point2D intersection = lineSegment.intersects();
//				intersections.add(intersection);	
//				for (lineSegment2D)
			}
		}
	}
	public static void main(String [] args) {
		System.out.println("The algothims begins to running!!!");
		SweepLine example = new SweepLine();
		example.solver();
		// test AVLTree
		
	}
}
