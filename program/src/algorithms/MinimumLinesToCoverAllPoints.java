package algorithms;
import java.util.ArrayList;
import utils.Pair;

public class MinimumLinesToCoverAllPoints {
	public int gcd(int a, int b) {
		if (b == 0)
			return a;
		return gcd(b, a % b);
	}
	
	public Pair getReducedForm(int dy, int dx) {
		int g = gcd(Math.abs(dx), Math.abs(dy));
		boolean sign = (dy < 0) ^ (dx < 0);
		
		if (sign)
			return new Pair(-Math.abs(dy) / g, Math.abs(dx) / g);
		else
			return new Pair(Math.abs(dy) / g, Math.abs(dx) / g);
	}
	
	public int solver(ArrayList<Pair> points, int N, int x0, int y0) {
		int minLines = 0;
		ArrayList<Pair> st = new ArrayList<Pair>();
		for (int i = 0; i < N; i++) {
			int curX = points.get(i).x;
			int curY = points.get(i).y;
			
			Pair temp = getReducedForm(curY - y0, curX - x0);
			if(!st.contains(temp)) {
				st.add(temp);
				minLines += 1;
			}
		}
		return minLines;
	}
}
