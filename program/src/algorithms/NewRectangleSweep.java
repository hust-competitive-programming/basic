package algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import math.AVLIntervalTree;
import math.Interval;
import math.Point2D;
import math.Rectangle;

public class NewRectangleSweep {
	public static class Event implements Comparable<Event>{
		int time;
		int rectangleID;
		int active;
		
		public Event(int time, int rec, int active) {
			this.time = time;
			this.rectangleID = rec;
			this.active = active;
		}

		@Override
		public int compareTo(Event other) {
			// TODO Auto-generated method stub
//			System.out.println(rectangleID + " " + other.rectangleID);
			if (this.time < other.time) return -1;
			else if (this.time > other.time) return 1;
			else  if(this.active == other.active) return 0;
			return 1;
		}
	}
	
	 public void BFS(int s, int t, ArrayList<ArrayList<Integer>> adjacencyLists) { 
        // Mark all the vertices as not visited(By default 
        // set as false) 
		int N = adjacencyLists.size();
        boolean visited[] = new boolean[N]; 
        int parrent[] =  new int[N];
  
        // Create a queue for BFS 
        LinkedList<Integer> queue = new LinkedList<Integer>(); 
  
        // Mark the current node as visited and enqueue it 
        visited[s]=true; 
        parrent[s]=-1;
        queue.add(s); 
  
        while (queue.size() != 0) 
        { 
            // Dequeue a vertex from queue and print it 
            s = queue.poll(); 
//            System.out.print(s+" "); 

//            Iterator<Integer> i = adj[s].listIterator(); 
            Iterator<Integer> i = adjacencyLists.get(s).listIterator();
            while (i.hasNext()) 
            { 
                int n = i.next(); 
                if (!visited[n]) 
                { 
                    visited[n] = true; 
                    parrent[n] = s;
                    if (n == t)
                    	break;
                    queue.add(n); 
                } 
            } 
        } 
        int trace = t;
        System.out.println(t);
        ArrayList<Integer> shortPath = new ArrayList<Integer>();
        while(parrent[trace] != -1) {
        	shortPath.add(trace);
        	trace = parrent[trace];
        }
        shortPath.add(s);
        Collections.reverse(shortPath);
        System.out.println(shortPath);      
    } 
	
	public void solver(ArrayList<Rectangle> rectangles) {
		int N = rectangles.size();
		PriorityQueue<NewRectangleSweep.Event> pq = new PriorityQueue<NewRectangleSweep.Event>();
		ArrayList<ArrayList<Integer>> adjacencyLists = new ArrayList<ArrayList<Integer>>();
		int[] isProcessed = new int[N];
		for (int i = 0; i < N; i++) {
			ArrayList<Integer> temp = new ArrayList<Integer>();
			temp.add(i);
			adjacencyLists.add(temp);
			isProcessed[i] = 0;
//			System.out.println(rectangles.get(i).topLeft.x + " " + rectangles.get(i).bottomRight.x);
			Event e1 = new Event(rectangles.get(i).topLeft.x, i, 0);
			Event e2 = new Event(rectangles.get(i).bottomRight.x, i, 1);
			pq.add(e1);
			pq.add(e2);
		}
//		Event e0 = pq.poll();
//		Event e1 = pq.poll();
//		Event e2 = pq.poll();
//		Event e3 = pq.poll();
//		
//		System.out.println(e0.rectangleID);
//		System.out.println(e1.rectangleID);
//		System.out.println(e2.rectangleID);
//		System.out.println(e3.rectangleID);
		
		AVLIntervalTree<String> st = new AVLIntervalTree<String>();
		while(!pq.isEmpty()) {
			Event e = pq.poll();
//			int sweep = e.time;
//			System.out.println(e);
			int id = e.rectangleID;
			if(isProcessed[id] == 0) {
//				System.out.println(rectangles.get(id).getInter().id + "->>>>>>>>>>>>>>>>>>>");
				Iterable<Interval> intersections = st.searchAll(rectangles.get(id).getInter());
//				System.out.println();
				for (Interval inter: intersections) {
//					System.out.print(inter.id + " ");
					adjacencyLists.get(rectangles.get(id).getInter().id).add(inter.id);
					adjacencyLists.get(inter.id).add(rectangles.get(id).getInter().id);
				}
				st.put(rectangles.get(id).getInter(), "" + id);
//				System.out.println("");
				isProcessed[id] = 1;
			}else st.remove(rectangles.get(id).getInter());
		}
////		System.out.println("/=======================================/");
//		for (int i = 0; i < N; i++) {
////			System.out.print(i + "->");
//			for(Integer i1: adjacencyLists.get(i)) {
//				System.out.print(i1.intValue() + "->");
//			}
//			System.out.println();
//		}
//		System.out.println("/=======================================/");
		BFS(0, 3, adjacencyLists);	
	}
	
	public static void main(String [] args) {
		ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();
		int N = 4;
		Rectangle rectangle0 = new Rectangle(new Point2D(0, 2), new Point2D(2, 0), 0);
		Rectangle rectangle2 = new Rectangle(new Point2D(2, 5), new Point2D(4, 0), 1);
		Rectangle rectangle1 = new Rectangle(new Point2D(1, 4), new Point2D(5, 1), 2);
		Rectangle rectangle3 = new Rectangle(new Point2D(3, 8), new Point2D(5, 3), 3);
		rectangles.add(rectangle0);
		rectangles.add(rectangle1);
		rectangles.add(rectangle2);
		rectangles.add(rectangle3);
		NewRectangleSweep test = new NewRectangleSweep();
		test.solver(rectangles);
	}
}
