package algorithms;
//import java.awt.geom.Line2D;

import java.util.ArrayList;

import math.Line2D;
import math.Point2D;

public class DetectingCollinearity {
	
	public static class triplePoint{
		Point2D[] triple = new Point2D[3];
		public triplePoint(Point2D p0, Point2D p1, Point2D p2) {
			triple[0] = p0;
			triple[1] = p1;
			triple[2] = p2;
		}
	}
	
	public static ArrayList<Point2D> triplePoints;
		
	public static ArrayList<Point2D> points;
	public int n;
	
	public ArrayList<Point2D> randomPoints(int n){
		ArrayList<Point2D> points = new ArrayList<Point2D>();
		for (int i = 0; i < n; i++) {
			Point2D newPoint = new Point2D(-10.0, 10.0, 0.0);
			points.add(newPoint);
		}
		return points;
	}
	
	public void solves(int n, ArrayList<Point2D> points) {
		if(points == null)
			points = randomPoints(n);
		ArrayList<Line2D> transformLines = new ArrayList<Line2D>();
		for(Point2D point: points) {
			Line2D transformLine = point.transforms();
			transformLines.add(transformLine);
		}
		if(checkIntersection(transformLines)) {
			System.out.println("YES");
		}else
			System.out.println("NO");
	}
	
	
	public boolean checkIntersection(ArrayList<Line2D> lines) {
		int n = lines.size();
		ArrayList<Point2D> intersections = new ArrayList<Point2D>();
		for(int i = 0; i < n - 1; i++)
			for(int j = i + 1; j < n; j++) {
				Point2D intersection = lines.get(i).getIntersection(lines.get(j));
				System.out.println(i + '-' + j);
				intersections.add(intersection);
			}
		n = intersections.size();
		for(int i = 0; i < n - 1; i++)
			for(int j = i; j < n; j++) {
				if(intersections.get(i).equals(intersections.get(j))) {
					return true;
				}
			}
		return false;
	}
	
	public static void main(String [] args) {
		DetectingCollinearity solver = new DetectingCollinearity();
		DetectingCollinearity.points = new ArrayList<Point2D>();
		DetectingCollinearity.points.add(new Point2D(0.0, 1.0));
		DetectingCollinearity.points.add(new Point2D(3.0, 2.0));
		DetectingCollinearity.points.add(new Point2D(1.0, 2.0));
		DetectingCollinearity.points.add(new Point2D(1.0, 0.0));
		DetectingCollinearity.points.add(new Point2D(0.0, -2.0));
		solver.solves(8, DetectingCollinearity.points);
	}
}
