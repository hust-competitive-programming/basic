package algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import math.AVLDoubleIntervalTree;
import math.DoubleInterval;
import math.DoublePoint2D;
import math.DoubleRectangle;
import math.LineSegment2D;
import math.Point2D;


public class SweepRectangle {
	public static class Event implements Comparable<Event>{
		Double time;
		int rectangleID;
		int active;
		
		public Event(Double time, int rec, int active) {
			this.time = time;
			this.rectangleID = rec;
			this.active = active;
		}

		@Override
		public int compareTo(Event other) {
			// TODO Auto-generated method stub
//			System.out.println(rectangleID + " " + other.rectangleID);
			if (this.time < other.time - 1.0e-9) return -1;
			else if (this.time > other.time + 1.0e-9) return 1;
			else  if(this.active == other.active) return 0;
			return 1;
		}
	}
	
	 public void BFS(int s, int t, ArrayList<ArrayList<Integer>> adjacencyLists) { 
        // Mark all the vertices as not visited(By default 
        // set as false) 
		int N = adjacencyLists.size();
        boolean visited[] = new boolean[N]; 
        int parrent[] =  new int[N];
  
        // Create a queue for BFS 
        LinkedList<Integer> queue = new LinkedList<Integer>(); 
  
        // Mark the current node as visited and enqueue it 
        visited[s]=true; 
        parrent[s]=-1;
        queue.add(s); 
  
        while (queue.size() != 0) 
        { 
            // Dequeue a vertex from queue and print it 
            s = queue.poll(); 
//            System.out.print(s+" "); 

//            Iterator<Integer> i = adj[s].listIterator(); 
            Iterator<Integer> i = adjacencyLists.get(s).listIterator();
            while (i.hasNext()) 
            { 
                int n = i.next(); 
                if (!visited[n]) 
                { 
                    visited[n] = true; 
                    parrent[n] = s;
                    if (n == t)
                    	break;
                    queue.add(n); 
                } 
            } 
        } 
        int trace = t;
        System.out.println(t);
        ArrayList<Integer> shortPath = new ArrayList<Integer>();
        while(parrent[trace] != -1) {
        	shortPath.add(trace);
        	trace = parrent[trace];
        }
        shortPath.add(s);
        Collections.reverse(shortPath);
        System.out.println(shortPath);      
    } 
	 
	public ArrayList<DoubleRectangle> randomData(int n, Double min, Double max, Double offset){
		ArrayList<DoubleRectangle> randomRectangles = new ArrayList<DoubleRectangle>();
		DoubleRectangle randomRectangle;
		for(int i = 0; i < n; i++) {
			Double x1 = (Double) (Math.random() * (max - min) + min);
			Double x2 = (Double) (Math.random() * (max - x1 - offset) + x1 + offset);
			Double y1 = (Double) (Math.random() * (max - min) + min);
			Double y2 = (Double) (Math.random() * (max - y1 - offset) + y1 + offset);
			randomRectangle = new DoubleRectangle(new DoublePoint2D(x1, y1), new DoublePoint2D(x2, y2), i);
//			else randomRectangle = new DoubleRectangle(new DoubleRectangle(new DoublePoint2(x1, y1), new DoublePoint2D(x2, y2));
			randomRectangles.add(randomRectangle);
		}
		return randomRectangles;
	}
	
	public void solver(ArrayList<DoubleRectangle> rectangles, int N) {
		if (rectangles == null) {
			rectangles = randomData(N, 0.0, 30.0, 3.0);
		}
//		int N = rectangles.size();
		PriorityQueue<SweepRectangle.Event> pq = new PriorityQueue<SweepRectangle.Event>();
		ArrayList<ArrayList<Integer>> adjacencyLists = new ArrayList<ArrayList<Integer>>();
		int[] isProcessed = new int[N];
		for (int i = 0; i < N; i++) {
			ArrayList<Integer> temp = new ArrayList<Integer>();
			temp.add(i);
			adjacencyLists.add(temp);
			isProcessed[i] = 0;
//			System.out.println(rectangles.get(i).topLeft.x + " " + rectangles.get(i).bottomRight.x);
			Event e1 = new Event(rectangles.get(i).topLeft.x, i, 0);
			Event e2 = new Event(rectangles.get(i).bottomRight.x, i, 1);
			pq.add(e1);
			pq.add(e2);
		}
//		Event e0 = pq.poll();
//		Event e1 = pq.poll();
//		Event e2 = pq.poll();
//		Event e3 = pq.poll();
//		
//		System.out.println(e0.rectangleID);
//		System.out.println(e1.rectangleID);
//		System.out.println(e2.rectangleID);
//		System.out.println(e3.rectangleID);
		
		AVLDoubleIntervalTree<String> st = new AVLDoubleIntervalTree<String>();
		while(!pq.isEmpty()) {
			Event e = pq.poll();
//			int sweep = e.time;
//			System.out.println(e);
			int id = e.rectangleID;
			if(isProcessed[id] == 0) {
//				System.out.println(rectangles.get(id).getInter().id + "->>>>>>>>>>>>>>>>>>>");
				Iterable<DoubleInterval> intersections = st.searchAll(rectangles.get(id).getInter());
//				System.out.println();
				for (DoubleInterval inter: intersections) {
//					System.out.print(inter.id + " ");
					adjacencyLists.get(rectangles.get(id).getInter().id).add(inter.id);
					adjacencyLists.get(inter.id).add(rectangles.get(id).getInter().id);
				}
				st.put(rectangles.get(id).getInter(), "" + id);
//				System.out.println("");
				isProcessed[id] = 1;
			}else st.remove(rectangles.get(id).getInter());
		}
////		System.out.println("/=======================================/");
//		for (int i = 0; i < N; i++) {
////			System.out.print(i + "->");
//			for(Integer i1: adjacencyLists.get(i)) {
//				System.out.print(i1.intValue() + "->");
//			}
//			System.out.println();
//		}
//		System.out.println("/=======================================/");
		BFS(0, 3, adjacencyLists);	
	}
	
	public static void main(String [] args) {
		ArrayList<DoubleRectangle> rectangles = new ArrayList<DoubleRectangle>();
		int N = 4;
		DoubleRectangle rectangle0 = new DoubleRectangle(new DoublePoint2D(0.0, 2.0), new DoublePoint2D(2.0, 0.0), 0);
		DoubleRectangle rectangle2 = new DoubleRectangle(new DoublePoint2D(2.0, 5.0), new DoublePoint2D(4.0, 0.0), 1);
		DoubleRectangle rectangle1 = new DoubleRectangle(new DoublePoint2D(1.0, 4.0), new DoublePoint2D(5.0, 1.0), 2);
		DoubleRectangle rectangle3 = new DoubleRectangle(new DoublePoint2D(3.0, 8.0), new DoublePoint2D(5.0, 3.0), 3);
		rectangles.add(rectangle0);
		rectangles.add(rectangle1);
		rectangles.add(rectangle2);
		rectangles.add(rectangle3);
		SweepRectangle test = new SweepRectangle();
		test.solver(rectangles, N);
	}
}
