package utils;

import java.util.ArrayList;
import math.LineSegment2D;

public class RangeSearch {
	
	public AVLTreeNode currentRoot;
	
	// A utility function to get the height of the tree 
	int height(AVLTreeNode N) { 
		if (N == null) 
			return 0; 

		return N.height; 
	} 
	
	// A utility function to right rotate subtree rooted with y 
	// See the diagram given above. 
	AVLTreeNode rightRotate(AVLTreeNode currentRoot) { 
		AVLTreeNode left = currentRoot.left; 
		AVLTreeNode rightLeft = left.right; 

		// Perform rotation 
		left.right = currentRoot; 
		currentRoot.left = rightLeft; 

		// Update heights 
		currentRoot.height = Math.max(height(currentRoot.left), height(currentRoot.right)) + 1; 
		left.height = Math.max(height(left.left), height(left.right)) + 1; 

		// Return new root 
		return left; 
	} 
		
	// A utility function to left rotate subtree rooted with x 
	// See the diagram given above. 
	AVLTreeNode leftRotate(AVLTreeNode currentRoot) { 
		AVLTreeNode right = currentRoot.right; 
		AVLTreeNode leftRight = right.left; 

		// Perform rotation 
		right.left = currentRoot; 
		currentRoot.right = leftRight; 
		
		// Update parents
//		right.parent = currentRoot.parent;
//		currentRoot.parent = right;
//		leftRight.parent = currentRoot;

		// Update heights 
		currentRoot.height = Math.max(height(currentRoot.left), height(currentRoot.right)) + 1; 
		right.height = Math.max(height(right.left), height(right.right)) + 1; 

		// Return new root 
		return right; 
	}
	
	// Get Balance factor of node N 
	int getBalance(AVLTreeNode N) { 
		if (N == null) 
			return 0; 

		return height(N.left) - height(N.right); 
	} 
	
	public AVLTreeNode insert(AVLTreeNode root, LineSegment2D key) {
		/* 1. Perform the normal BST insertion */
		if (root == null) 
			return (new AVLTreeNode(key)); 
		
		if (key.comparesAccordingToRightPoint(root.key))
			root.left = insert(root.left, key);
		else if (root.key.comparesAccordingToRightPoint(key))
			root.right = insert(root.right, key);
		else 
			return root;
		
		/* 2. Update height of this ancestor node */
		root.height = 1 + Math.max(height(root.left), 
							height(root.right)); 
		/* 3. Get the balance factor of this ancestor 
		node to check whether this node became 
		unbalanced */
		int balance = getBalance(root); 
	
		// If this node becomes unbalanced, then there 
		// are 4 cases Left Left Case 
		if (balance > 1 && key.comparesAccordingToRightPoint(root.left.key)) 
			return rightRotate(root); 
	
		// Right Right Case 
		if (balance < -1 && root.right.key.comparesAccordingToRightPoint(key)) 
			return leftRotate(root); 
	
		// Left Right Case 
		if (balance > 1 && key.comparesAccordingToRightPoint(root.left.key)) { 
			root.left = leftRotate(root.left); 
			return rightRotate(root); 
		} 
	
		// Right Left Case 
		if (balance < -1 && root.right.key.comparesAccordingToRightPoint(key)) { 
			root.right = rightRotate(root.right); 
			return leftRotate(root); 
		} 
	
		/* return the (unchanged) node pointer */
		return root;
	}
	
	public AVLTreeNode minValueNode(AVLTreeNode node) 
	{ 
		AVLTreeNode current = node; 

		/* loop down to find the leftmost leaf */
		while (current.left != null) 
		current = current.left; 

		return current; 
	}
	
	public AVLTreeNode remove(AVLTreeNode root, LineSegment2D key) 
	{ 
		// STEP 1: PERFORM STANDARD BST DELETE 
		if (root == null) 
			return root; 

		// If the key to be deleted is smaller than 
		// the root's key, then it lies in left subtree 
		if (key.comparesAccordingToRightPoint(root.key)) 
			root.left = remove(root.left, key); 

		// If the key to be deleted is greater than the 
		// root's key, then it lies in right subtree 
		else if (root.key.comparesAccordingToRightPoint(key)) 
			root.right = remove(root.right, key); 

		// if key is same as root's key, then this is the node 
		// to be deleted 
		else
		{ 

			// node with only one child or no child 
			if ((root.left == null) || (root.right == null)) 
			{ 
				AVLTreeNode temp = null; 
				if (temp == root.left) 
					temp = root.right; 
				else
					temp = root.left; 

				// No child case 
				if (temp == null) 
				{ 
					temp = root; 
					root = null; 
				} 
				else // One child case 
					root = temp; // Copy the contents of 
								// the non-empty child 
			} 
			else
			{ 

				// node with two children: Get the inorder 
				// successor (smallest in the right subtree) 
				AVLTreeNode temp = minValueNode(root.right); 

				// Copy the inorder successor's data to this node 
				root.key = temp.key; 

				// Delete the inorder successor 
				root.right = remove(root.right, temp.key); 
			} 
		} 

		// If the tree had only one node then return 
		if (root == null) 
			return root; 

		// STEP 2: UPDATE HEIGHT OF THE CURRENT NODE 
		root.height = Math.max(height(root.left), height(root.right)) + 1; 

		// STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether 
		// this node became unbalanced) 
		int balance = getBalance(root); 

		// If this node becomes unbalanced, then there are 4 cases 
		// Left Left Case 
		if (balance > 1 && getBalance(root.left) >= 0) 
			return rightRotate(root); 

		// Left Right Case 
		if (balance > 1 && getBalance(root.left) < 0) 
		{ 
			root.left = leftRotate(root.left); 
			return rightRotate(root); 
		} 

		// Right Right Case 
		if (balance < -1 && getBalance(root.right) <= 0) 
			return leftRotate(root); 

		// Right Left Case 
		if (balance < -1 && getBalance(root.right) > 0) 
		{ 
			root.right = rightRotate(root.right); 
			return leftRotate(root); 
		} 

		return root; 
	} 
	// A utility function to print preorder traversal 
	// of the tree. 
	// The function also prints height of every node 
	public void preOrder(AVLTreeNode node) { 
		if (node != null) { 
			System.out.print(node.key.id + " "); 
			preOrder(node.left); 
			preOrder(node.right); 
		} 
	} 
	
//	public ArrayList<LineSegment2D> getListNodeFromRoot(LineSegment2D lineSeg){
//		ArrayList<LineSegment2D> lineSegments = new ArrayList<LineSegment2D>();
//		while()
//	}
	
	public AVLTreeNode getNodeByLineSegment(AVLTreeNode root, LineSegment2D lineSeg) {
		if (root.key.equals(lineSeg))
			return root;
		if (lineSeg.comparesAccordingToRightPoint(root.key))
			return getNodeByLineSegment(root.left, lineSeg);
		else 
			return getNodeByLineSegment(root.right, lineSeg);
	}
	
	public AVLTreeNode getRootBetweenTwoGivenNode(AVLTreeNode root, AVLTreeNode lineSeg1, AVLTreeNode lineSeg2) {
		if (root.key.comparesAccordingToRightPoint(lineSeg2.key) && lineSeg1.key.comparesAccordingToRightPoint(root.key))
			return root;
		if (root.left.key.comparesAccordingToRightPoint(lineSeg1.key))
			return getRootBetweenTwoGivenNode(root.right, lineSeg1, lineSeg2);
		if (lineSeg2.key.comparesAccordingToRightPoint(root.right.key))
			return getRootBetweenTwoGivenNode(root.left, lineSeg1, lineSeg2);
		return null;
	}
	
	public boolean check(AVLTreeNode root, AVLTreeNode lineSeg1, AVLTreeNode lineSeg2) {
		return root.equals(lineSeg1) && root.equals(lineSeg2);
	}
	
	public void getRange(ArrayList<LineSegment2D> lineSegments, AVLTreeNode root, AVLTreeNode lineSeg1, AVLTreeNode lineSeg2){
		if(check(root, lineSeg1, lineSeg2))
			return;
		getRange(lineSegments, root.left, lineSeg1, lineSeg2);
		lineSegments.add(root.key);
		getRange(lineSegments, root.right, lineSeg1, lineSeg2);
	}
	
	public ArrayList<LineSegment2D> range(AVLTreeNode root, LineSegment2D lineSeg1, LineSegment2D lineSeg2) {
		ArrayList<LineSegment2D> lineSegments = new ArrayList<LineSegment2D>();
		AVLTreeNode node1 = getNodeByLineSegment(root, lineSeg1);
		AVLTreeNode node2 = getNodeByLineSegment(root, lineSeg2);
		AVLTreeNode rootOfTwoNode = getRootBetweenTwoGivenNode(root, node1, node2);
		
		getRange(lineSegments, rootOfTwoNode, node1, node2);
		
		return lineSegments;
	}
	
//	public ArrayList<LineSegment2D> getRangeByPoints(AVLTreeNode root, Point2D p1, Point2D p2){
//		ArrayList<LineSegment2D> lineSegments = new ArrayList<LineSegment2D>();
//		
//	}

}
