package utils;
import math.LineSegment2D;

public class AVLTreeNode {
	
	LineSegment2D key;
	int height;
	AVLTreeNode left, right;
//	public Point2D rightPoint;
	
	public AVLTreeNode(LineSegment2D d){
		key = d;
		height = 1;
	}
	
	public AVLTreeNode minNode(AVLTreeNode other) {
		if (key.comparesAccordingToRightPoint(other.key))
			return this;
		return other;
	}
	
	public boolean equals(AVLTreeNode other) {
		return key.equals(other.key);
	}
}
