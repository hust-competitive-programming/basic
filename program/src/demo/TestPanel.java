package demo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.awt.geom.Line2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import algorithms.SweepRectangle;
import math.DoublePoint2D;
import math.DoubleRectangle;

public class TestPanel extends JPanel {
	static ArrayList<DoubleRectangle> rectangles;
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int N = rectangles.size();
        for (int i = 0; i < N; i++) {
            draw(g, rectangles.get(i));
        }
        DoublePoint2D last; 
//        last = rectangles.get(0).getPointCentre();
//        for (int i = 1; i < N; i++) {
//            drawLine(g, last, rectangles.get(i).getPointCentre());
//            last = rectangles.get(i).getPointCentre();
//        }
//        for (int i = 0; i < N; i++) {
//          drawLine(g, rectangles.get(i).topLeft, rectangles.get(i).bottomRight);
////          last = rectangles.get(i).getPointCentre();
//        }
    }
    
    public void drawLine(Graphics g, DoublePoint2D last, DoublePoint2D current) {
    	Color c = new Color(255, 0, 255);
    	g.setColor(c);
    	g.drawLine(last.x.intValue(), last.y.intValue(), current.x.intValue(), current.y.intValue());
    	Color c1 = new Color(255, 44, 255);
    	g.setColor(c1);
    	g.drawLine(last.x.intValue(), current.y.intValue(), current.x.intValue(), last.y.intValue());
    }

    public void draw(Graphics g, DoubleRectangle rectangle) {
        Color c = new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
        g.setColor(c);
        g.fillRect(rectangle.topLeft.x.intValue(), rectangle.topLeft.x.intValue(), rectangle.getWidth().intValue(), rectangle.getHeight().intValue());
        g.drawLine(0, 0, rectangle.getPointCentre().x.intValue(), rectangle.getPointCentre().y.intValue());
    }

    public static void main(String[] args) {
    	SweepRectangle test = new SweepRectangle();
    	rectangles = test.randomData(5, 0.0, 200.0, 3.0);
    	int N = 5;
    	for (int i = 0; i < N; i++) {
    		Color c = new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
    		System.out.println("context.fillStyle = 'rgb(" + Math.floor(Math.random() * 255) + ","+ Math.floor(Math.random() * 255)  + "," + Math.floor(Math.random() * 255) + ")'");
            System.out.println("context.fillRect("+rectangles.get(i).topLeft.x +", " + rectangles.get(i).topLeft.y+", " + rectangles.get(i).bottomRight.x+", " + rectangles.get(i).bottomRight.y+")");
            System.out.println("context.strokeText(" + i +  ", " + (rectangles.get(i).topLeft.x + 10) +  ", " + (rectangles.get(i).topLeft.y + 10) + ")");
            System.out.println("context.strokeText(" + i +  ", " + (rectangles.get(i).topLeft.x + 10) +  ", " + (rectangles.get(i).bottomRight.y + 10) + ")");
            System.out.println("context.strokeText(" + i +  ", " + (rectangles.get(i).bottomRight.x + 10) +  ", " + (rectangles.get(i).topLeft.y + 20) + ")");
            System.out.println("context.strokeText(" + i +  ", " + (rectangles.get(i).bottomRight.x + 10) +  ", " + (rectangles.get(i).bottomRight.y + 20) + ")");
        }
    	
//		test.solver(null, 10);
		
//        JFrame f = new JFrame();
//        f.getContentPane().add(new TestPanel(), BorderLayout.CENTER);
//        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        f.setSize(300, 500);
//        f.setVisible(true);
    }
}